package net.alextwelshie.deathdrop.achievements;

public enum Achievement {
    
    FIRSTJOIN,
    PICKBLOCK,
    FIRST_LAND_SUCCESS,
    FIRST_LAND_FAIL,
    GOODGAME,
    COMPLETED;
    
}
