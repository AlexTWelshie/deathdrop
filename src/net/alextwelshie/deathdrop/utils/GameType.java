package net.alextwelshie.deathdrop.utils;

public enum GameType {

    Enhanced,
    Normal;
}
