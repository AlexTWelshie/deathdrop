package net.alextwelshie.deathdrop.utils;

public enum GameState {

    LOBBY,
    INGAME,
    RESTARTING;
}
